from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from PIL import Image
from math import sqrt

import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs

'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def plot_mul(c, D, im_num, X_mn, num_coeffs, n_blocks):
    '''
    Plots nine PCA reconstructions of a particular image using number
    of components specified by num_coeffs

    Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the image blocks.
        n represents the maximum dimension of the PCA space.
        m is (number of images x n_blocks**2)

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)

    im_num: Integer
        index of the image to visualize

    X_mn: np.ndarray
        a matrix representing the mean block.

    num_coeffs: Iterable
        an iterable with 9 elements representing the number_of coefficients
        to use for reconstruction for each of the 9 plots

    n_blocks: Integer
        number of blocks comprising the image in each direction.
        For example, for a 256x256 image divided into 64x64 blocks, n_blocks will be 4
    '''
    #print c.shape
    #print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^6       "
    f, axarr = plt.subplots(3, 3)
    for i in range(3):
        for j in range(3):
            nc = num_coeffs[i*3+j]
            cij = c[:nc, n_blocks*n_blocks*im_num:n_blocks*n_blocks*(im_num+1)]
            Dij = D[:, :nc]
            #print n_blocks*n_blocks*im_num,"      %%%    ",n_blocks*n_blocks*(im_num+1), "   $$$"
            print "******************************************       "
            print cij.shape,"          ",Dij.shape,"          ",im_num,"          ",n_blocks,"          ",X_mn.shape
            plot(cij, Dij, n_blocks, X_mn, axarr[i, j])
            #raise NotImplementedError

    f.savefig('backup/hw1a_{0}_im{1}.png'.format(n_blocks, im_num))
    plt.close(f)

def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image block of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of each block

    imname: string
        name of file where image will be saved.
    '''
    #get top 16 vectors
    print "D shape:  ", D.shape
    print "sz:  ", sz   #  16
    (D_height,D_length) = D.shape # 256*256
    pix_per_block = D_height
    
    n_blocks_total = 16
    n_blocks = int(sqrt(n_blocks_total)) # 4
    count_rec = 0
    print "n_blocks:  ", n_blocks
    top_16 = D[:,0:16]
    print top_16.shape
    #print "top_16 before:  ", top_16
    top_16 = top_16.ravel(order='F')
    #print "top_16 before:  ", top_16
    new_block = np.zeros((sz,sz))
    final_im = np.zeros((sz*n_blocks,sz*n_blocks))   #  (16*4) * (16*4) = 4096 = 16*256
    
    for q in range(0,n_blocks):   
        for k in range(0,n_blocks):
            for j in range(0,sz):
                for i in range(0,sz):
                    new_block[j,i] = top_16[count_rec]
                    count_rec += 1
            final_im[sz*q:sz*q+sz , sz*k:sz*k+sz] = new_block #+ X_mn[q,k]
        
    get2 = plt.figure()
    plt.imshow(final_im, cmap = cm.Greys_r)
    get2.savefig(imname)
    
    
    #raise NotImplementedError


def plot(c, D, n_blocks, X_mn, ax):
    '''
    Plots a reconstruction of a particular image using D as the basis matrix and coeffiecient
    vectors from c

    Parameters
    ------------------------
        c: np.ndarray
            a l x m matrix  representing the coefficients of all blocks in a particular image
            l represents the dimension of the PCA space used for reconstruction
            m represents the number of blocks in an image

        D: np.ndarray
            an N x l matrix representing l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in a block)

        n_blocks: Integer
            number of blocks comprising the image in each direction.
            For example, for a 256x256 image divided into 64x64 blocks, n_blocks will be 4

        X_mn: basis vectors represent the divergence from the mean so this
            matrix should be added to all reconstructed blocks

        ax: the axis on which the image will be plotted
    '''
    reconstr = np.dot(D,c) #+ X_mn
    #reshape into image
    reconstr = reconstr.ravel(order='F') #256^2 in size
    im_size = 256
    block_size = im_size/n_blocks # 256/4 = 64
    new_block = np.zeros((block_size,block_size))
    final_im = np.zeros((im_size,im_size))
    count_rec = 0
    
    for q in range(0,n_blocks):   
        for k in range(0,n_blocks):
            for j in range(0,block_size):
                for i in range(0,block_size):
                    new_block[j,i] = reconstr[count_rec]
                    count_rec += 1
            final_im[block_size*q:block_size*q+block_size , block_size*k:block_size*k+block_size] = new_block + X_mn[q,k]
        
    
    ax.imshow(final_im, cmap = cm.Greys_r)


def main():
    '''
    Read here all images(grayscale) from jaffe folder
    into an numpy array Ims with size (no_images, height, width).
    Make sure the images are read after sorting the filenames
    '''
    directory = "jaffe"
    #Get file names
    f = []
    for (dirpath, dirnames, filenames) in walk(directory):
        f.extend(filenames)
        break
    #get array dimensions
    im = np.array( Image.open(directory + "/" + f[0]))
    (height,width) = im.shape
    no_images = len(f)
    print no_images
    #read files into 3d array
    Im = np.zeros( (no_images, height, width) )
    for i in range(0, no_images):
        Im[i,:,:] = Image.open(directory + "/" + f[i])
        
    szs = [16, 32, 64]
    num_coeffs = [range(1, 10, 1), range(3, 30, 3), range(5, 50, 5)]

    #run only once: size 16, coeffs 1
    for sz, nc in zip(szs, num_coeffs):
        '''
        Divide here each image into non-overlapping blocks of shape (sz, sz).
        Flatten each block and arrange all the blocks in a
        (no_images*n_blocks_in_image) x (sz*sz) matrix called X
        '''
        n_blocks_per_side = int(height/sz)
        n_blocks_in_image = pow(n_blocks_per_side,2)
        X = np.zeros( (no_images*n_blocks_in_image , sz*sz) )
        count_block = 0
        print n_blocks_per_side, n_blocks_in_image
        print "X dim: ",X.shape
        
        print sz, height, n_blocks_per_side
        #loop through all images
        for j in range(0, no_images):
            #loop though all blocks
            for i in range(0, n_blocks_per_side):
                index_begin_y = sz*i
                index_end_y = sz*i + sz
                for k in range(0, n_blocks_per_side):
                    index_begin_x = sz*k
                    index_end_x = sz*k + sz
                    block = Im[j,index_begin_y:index_end_y,index_begin_x:index_end_x]
                    X[count_block,:] = block.ravel()
                    count_block +=1
        
        print "block dim: ",block.shape

        X_mn = np.mean(X, 0)
        X = X - np.repeat(X_mn.reshape(1, -1), X.shape[0], 0)

        '''
        Perform eigendecomposition on X^T X and arrange the eigenvectors
        in decreasing order of eigenvalues into a matrix D
        '''
        # Eigendecomposition of  X^T*X
        [w,v] = np.linalg.eigh(np.dot(X.T,X))
        #sort from least to most
        sorted_w = np.argsort(w)
        D = np.zeros( v.shape )        

        (v_rows,v_cols) = v.shape
        for i in range(0,v_cols):
            D[:,i] = v[:,v_cols-i-1]  
        
        print "v dim: ",v.shape
        print "X_mn: ",X_mn.shape
        c = np.dot(D.T, X.T)


        for i in range(0, 200, 10):
            pass
            plot_mul(c, D, i, X_mn.reshape((sz, sz)),
                     num_coeffs=nc, n_blocks=int(256/sz))
        
        plot_top_16(D, sz, imname='backup/hw1a_top16_{0}.png'.format(sz))


if __name__ == '__main__':
    main()