from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from math import sqrt

from PIL import Image

import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs

'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def plot_mul(c, D, im_num, X_mn, num_coeffs):
    '''
    Plots nine PCA reconstructions of a particular image using number
    of components specified by num_coeffs

    Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the images
        n represents the maximum dimension of the PCA space.
        m represents the number of images

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)

    im_num: Integer
        index of the image to visualize

    X_mn: np.ndarray
        a matrix representing the mean image
    '''
    f, axarr = plt.subplots(3, 3)

    for i in range(3):
        for j in range(3):
            nc = num_coeffs[i*3+j]
            cij = c[:nc, im_num]
            Dij = D[:, :nc]
            plot(cij, Dij, X_mn, axarr[i, j])

    f.savefig('output/hw1b_im{0}.png'.format(im_num))
    plt.close(f)


def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of a image

    imname: string
        name of file where image will be saved.
    '''
    #get top 16 vectors
    print "D shape:  ", D.shape
    print "sz:  ", sz   #  256
    (D_height,D_length) = D.shape # 65000*16
    pix_per_block = D_height
    
    n_blocks_total = 16
    n_blocks = int(sqrt(n_blocks_total)) # 4
    count_rec = 0
    print "n_blocks:  ", n_blocks
    top_16 = D[:,0:16]
    print top_16.shape
    #print "top_16 before:  ", top_16
    top_16 = top_16.ravel(order='F')
    #print "top_16 before:  ", top_16
    new_block = np.zeros((sz,sz))
    final_im = np.zeros((sz*n_blocks,sz*n_blocks))   #  (256*4) * (256*4) 
    
    for q in range(0,n_blocks):   
        for k in range(0,n_blocks):
            for j in range(0,sz):
                for i in range(0,sz):
                    new_block[j,i] = top_16[count_rec]
                    count_rec += 1
            final_im[sz*q:sz*q+sz , sz*k:sz*k+sz] = new_block #+ X_mn[q,k]
        
    get3 = plt.figure()
    plt.imshow(final_im, cmap = cm.Greys_r)
    get3.savefig(imname)


def plot(c, D, X_mn, ax):
    '''
    Plots a reconstruction of a particular image using D as the basis matrix and c as
    the coefficient vector
    Parameters
    -------------------
        c: np.ndarray
            a l x 1 vector  representing the coefficients of the image.
            l represents the dimension of the PCA space used for reconstruction

        D: np.ndarray
            an N x l matrix representing first l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in the image)

        X_mn: basis vectors represent the divergence from the mean so this
            matrix should be added to the reconstructed image

        ax: the axis on which the image will be plotted
    '''
    reconstr = np.dot(D,c) #+ X_mn
    #reshape into image
    reconstr = reconstr.ravel(order='F') #256^2 in size
    im_size = 256
    n_blocks = 1
    block_size = im_size/n_blocks # 256/4 = 64
    new_block = np.zeros((block_size,block_size))
    final_im = np.zeros((im_size,im_size))
    count_rec = 0
    
    for q in range(0,n_blocks):   
        for k in range(0,n_blocks):
            for j in range(0,block_size):
                for i in range(0,block_size):
                    new_block[j,i] = reconstr[count_rec]
                    count_rec += 1
            final_im[block_size*q:block_size*q+block_size , block_size*k:block_size*k+block_size] = new_block #+ X_mn[q,k]
        
    final_im += X_mn
    ax.imshow(final_im, cmap = cm.Greys_r)



if __name__ == '__main__':
    '''
    Read all images(grayscale) from jaffe folder and collapse each image
    to get an numpy array Ims with size (no_images, height*width).
    Make sure to sort the filenames before reading the images
    '''
    
    directory = "jaffe"
    #Get file names
    f = []
    for (dirpath, dirnames, filenames) in walk(directory):
        f.extend(filenames)
        break
    #get array dimensions
    im = np.array( Image.open(directory + "/" + f[0]))
    (height,width) = im.shape
    no_images = len(f)
    #read files into 2d array
    Ims = np.zeros( (no_images, height*width) )
    for i in range(0, no_images):
        flat_im = Image.open(directory + "/" + f[i])
        Ims[i,:] = np.ravel(flat_im)
        

    Ims = Ims.astype(np.float32)
    X_mn = np.mean(Ims, 0)
    X = Ims - np.repeat(X_mn.reshape(1, -1), Ims.shape[0], 0)

    '''
    Use theano to perform gradient descent to get top 16 PCA components of X
    Put them into a matrix D with decreasing order of eigenvalues

    If you are not using the provided AMI and get an error "Cannot construct a ufunc with more than 32 operands" :
    You need to perform a patch to theano from this pull(https://github.com/Theano/Theano/pull/3532)
    Alternatively you can downgrade numpy to 1.9.3, scipy to 0.15.1, matplotlib to 1.4.2
    '''

 
    #x = T.matrix("x")
    x = X
    (X_height,X_length) = X.shape   # 213 * 65,536
    #y = T.matrix("y")
    y = np.zeros((X_length, ))
    #d = T.vector("d")
    #A = T.matrix("A")
    Ai = 0
    #lam = T.vector("lam")
    lam = np.zeros(16)
    
    D = np.zeros((X_length,16))     # 65,536 * 16
    (D_rows,D_cols) = D.shape
    #D_transp = D.T
    N = D_cols
    learning_rate = 1
    max_steps = 100
    stop = False
    
    sum_dj = Ai
    di = np.zeros((X_length,))
    
    #only 1 run
    for i in range(0,N):
    # 1
    #for i in range(0,1):
        di = np.random.rand(X_length,)
        t = 1
        sum_dj = 0
        for j in range(0,i-1):
            sum_dj += lam[j]
        while ((t<max_steps) and (stop != True)):
            Xd = np.dot(X,di)
            dXT = np.dot(di.T,X.T)
            grad_1 = 2*( np.dot(Xd,dXT)  )
            y = di + learning_rate*grad_1
            y = y - sum_dj*( np.dot(Xd,dXT)  )
            di = y / np.linalg.norm(y)
            t += 1
        term1 = np.dot(di.T,x.T)
        term2 = np.dot(X,di)
        lam[i] = np.dot(term1,term2)
        print di[0:10]
        D[:,i] = di
        
    print "size of D: ", D.shape
        
    c = np.dot(D.T, X.T)    
    
    
    
    
    for i in range(0,200,10):
        plot_mul(c, D, i, X_mn.reshape((256, 256)),
                 [1, 2, 4, 6, 8, 10, 12, 14, 16])
        

    plot_top_16(D, 256, 'output/hw1b_top16_256.png')

